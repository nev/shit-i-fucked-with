# shit-i-fucked-with

A list of various things I install or modify after a fresh Linux install.

## Install

### apt

```
sudo apt install \
# System utilities & command-line stuff \
	acpi \
    caca-utils \
	dconf-editor \
	ffmpeg \
	git \
    ibus ibus-m17n \
	jhead \
	lynx \
	php-cli \
	php-curl \
	php-mbstring \
	php-zip \
    pipx \
	rclone \
	tilde \
	xclip \
# Graphical applications \
    geary \
	gimp \
    plank \
	synapse \
	vlc
```

### Python

```
pipx install \
    toot
    yt-dlp
```

### Other

Microsoft fonts (opens prompt window in terminal): 

```
sudo apt install ttf-mscorefonts-installer
```

Dropbox: https://www.dropbox.com/install-linux

If necessary: https://metabubble.net/linux/how-to-keep-using-dropbox-even-if-you-dont-use-unencrypted-ext4-workaround/

(Note: systray icon requires some finagling to work in elementary OS, see following sections.)

## Other configuration

### Replace ugly ibus icon for Yiddish

```
sudo mv /usr/share/m17n/icons/yi-yivo.png /usr/share/m17n/icons/yi-yivo.png.bak
```

Edit icon as desired, e. g. in GNU IMP, and then do `sudo mv ~/Pictures/yi-yivo_edited.png /usr/share/m17n/yi-yivo.png`

### ThinkPad trackpoint settings

```
sudo -i tilde /etc/tmpfiles.d/tpoint.conf
```

In the file:

> w /sys/devices/platform/i8042/serio1/serio2/speed - - - - 200  
> w /sys/devices/platform/i8042/serio1/serio2/sensitivity - - - - 200  

To apply without rebooting: 

```
sudo systemd-tmpfiles --prefix=/sys --create
```

Sources:

- http://web.archive.org/web/20201218222338/nrvale0.github.io/posts/adjusting-thinkpad-trackpoint-sensitivity-on-ubuntu-1604-and-others/
- https://ubuntuforums.org/showthread.php?t=1422202
- https://wiki.archlinux.org/index.php/TrackPoint

### Make bash autocompletion case-insensitive

Add to `.bashrc`: 

> bind -s 'set completion-ignore-case on'

Source: https://unix.stackexchange.com/a/686484

(Note: may break things)

### Terminal colour scheme

Default: 

```
'#073642:#dc322f:#859900:#b58900:#268bd2:#ec0048:#2aa198:#94a3a5:#586e75:#cb4b16:#859900:#b58900:#268bd2:#d33682:#2aa198:#6c71c4'
```

Dark solarized:

FG #1ABC9C
BG #1F2D3A

```
'#2c3e50:#c0392b:#27ae60:#f39c12:#2980b9:#8e44ad:#16a085:#bdc3c7:#34495e:#e74c3c:#2ecc71:#f1c40f:#3498db:#9b59b6:#2AA198:#ecf0f1'
```

Dracula (https://github.com/dracula/pantheon-terminal/):

FG #F8F8F2
BG #282A36

```
'#262626:#E356A7:#42E66C:#E4F34A:#9B6BDF:#E64747:#75D7EC:#EFA554:#7A7A7A:#FF79C6:#50FA7B:#F1FA8C:#BD93F9:#FF5555:#8BE9FD:#FFB86C'
```

## OS-specific tweaks

### LMDE 

Applets to install:

- Automatic dark/light themes

#### Don't use Mint's built-in apt wrapper

In `.bash_aliases`, add 

```
alias apt='/usr/bin/apt'
```

### Ubuntu

#### Disable Ubuntu Pro spam

```
sudo pro config set apt_news=false
```

```
sudo mv /etc/apt/apt.conf.d/20apt-esm-hook.conf /etc/apt/apt.conf.d/20apt-esm-hook.conf.disabled
```

Source: https://github.com/Skyedra/UnspamifyUbuntu

#### Install Firefox as a .deb, not a snap

```
sudo add-apt-repository ppa:mozillateam/ppa
echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001
' | sudo tee /etc/apt/preferences.d/mozilla-firefox
echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
sudo apt install firefox
```

### elementary OS

#### Wingpanel indicator support

```
sudo apt-get install libglib2.0-dev libgranite-dev libindicator3-dev libwingpanel-dev indicator-application
wget -c https://github.com/Lafydev/wingpanel-indicator-ayatana/raw/master/com.github.lafydev.wingpanel-indicator-ayatana_2.0.8_odin.deb
sudo apt install ./com.github.lafydev.wingpanel*.deb
sudo tilde /etc/xdg/autostart/indicator-application.desktop
```
Add "Pantheon;" after "OnlyShowIn=Unity;GNOME;", save and exit. Reboot.

(May also need to install `libgdk-pixbuf-2.0-0`; see any error messages.)

Source: https://elementaryos.stackexchange.com/a/28525/26780

#### Get /home partition to show up in Files

```
sudo tilde /etc/fstab
```

Add "x-gvfs-show" to options for `/home` partition

Source: https://elementaryos.stackexchange.com/a/9931/26780

Rename it in bookmarks: `~/.config/gtk-3.0/bookmarks`

#### Disable Mail auto-starting

Mail is buggy, I'm using Geary instead.

```
mv ~/.config/autostart/pantheon-mail-autostart.desktop ~/.config/autostart/pantheon-mail-autostart.desktop.bak
sudo mv /etc/xdg/autostart/io.elementary.mail-daemon.desktop /etc/xdg/autostart/io.elementary.mail-daemon.desktop.bak
```

#### Nuke fugly croscore fonts

```
sudo mv /etc/fonts/conf.avail/31-croscore-elementary.conf /etc/fonts/conf.avail/31-croscore-elementary.conf.bak
```

### KDE

#### Get global menu/appmenu to work with Firefox

```
sudo tilde /etc/apt/sources.list.d/bionic.list
```

> deb http://archive.ubuntu.com/ubuntu/ bionic main restricted universe multiverse  
> deb http://security.ubuntu.com/ubuntu/ bionic-security main restricted universe multiverse  
> deb http://archive.ubuntu.com/ubuntu/ bionic-updates main restricted universe multiverse  

```
sudo tilde /etc/apt/preferences.d/bionic.pref
```

> #preference for firefox from bionic  
>	Explanation: Allow installing firefox from bionic  
>	Package: firefox  
>	Pin: release a=bionic-updates  
>	Pin-Priority: 1101  
>  
>	Explanation: Avoid other packages from the bionic repo.  
>	Package: *  
>	Pin: release a=bionic  
>	Pin-Priority: 1  

To test: 

```
apt -s full-upgrade
```

Source: https://askubuntu.com/a/1304485 
